# Deep Profiler Database Uploader

This program uploads real-time Deep Profiler data records from the Docking
Station Controller (DSC) to
an [InfluxDB](https://docs.influxdata.com/influxdb/v1.2/introduction/)
time-series database.

## Data Source

The DPC monitoring program publishes the data records received from the
Profiler to a couple of [Redis](https://redis.io) Pub-sub channels
(*data.sci* and *data.eng*) in JSON format.

## Operations

This program should be run as a service by a supervisor such
as [Runit](http://smarden.org/runit/index.html).

### Usage

``` shellsession
$ dpdbstore --help

Usage: dpdbstore [options] DBURL DBNAME

Read DP data from multiple Redis pub-sub channels and write the
data records to an InfluxDB database DBNAME at DBURL
  -dchans string
        Comma separated list of data pubsub channels (default "data.sci,data.eng")
  -evchan string
        Profile events pubsub channel (default "events.mmp")
  -pnum int
        Initial profile number
  -pword string
        Database password
  -tags string
        Tags to add to every entry
  -user string
        Database user name
  -version
        Show program version information and exit
```
