// Dpdbstore uploads real-time Deep Profiler data from the DSC to
// an InfluxDB time-series database.
package main

import (
	"bitbucket.org/uwaploe/go-dputil"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/influxdata/influxdb/client/v2"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
)

var Version = "dev"
var BuildDate = "unknown"

// Reformat a profiler data record
func convert_profiler(data map[string]interface{}) {
	var scaled = map[string]float64{
		"pressure":      0.001,
		"voltage":       0.001,
		"current":       0.001,
		"energy":        0.001,
		"motor_current": 0.001,
		"vmon":          0.001,
		"humidity":      0.1,
		"rel_charge":    1.0,
	}

	// Convert all scaled integers to floats
	for k, v := range scaled {
		if _, ok := data[k]; ok {
			data[k] = data[k].(float64) * v
		}
	}

	// Make sure the profile number is a signed integer
	data["profile"] = int64(data["profile"].(float64))

	// Expand the list of internal temperature values
	for i, val := range data["itemp"].([]interface{}) {
		data[fmt.Sprintf("itemp_%d", i)] = val.(float64) * float64(0.1)
	}

	// Delete the list.
	delete(data, "itemp")
}

// Reformat a DSC data record
func convert_dock(data map[string]interface{}) {
	var scaled = map[string]float64{
		"T_ambient":  0.001,
		"T_heatsink": 0.001,
		"humidity":   0.1,
		"v12":        0.001,
	}

	// Convert all scaled integers to floats
	for k, v := range scaled {
		if _, ok := data[k]; ok {
			data[k] = data[k].(float64) * v
		}
	}

	data["ips_status"] = int64(data["ips_status"].(float64))
}

// Convert all data record fields to integers
func convert_to_int(data map[string]interface{}) {
	for k, v := range data {
		data[k] = int64(v.(float64))
	}
}

// Reformat each data record according to its source
func reformat_data(dr *dputil.DataRecord) {
	switch dr.Source {
	case "profiler":
		convert_profiler(dr.Data.(map[string]interface{}))
	case "dock":
		convert_dock(dr.Data.(map[string]interface{}))
	case "flntu_1":
		convert_to_int(dr.Data.(map[string]interface{}))
	case "flcd_1":
		convert_to_int(dr.Data.(map[string]interface{}))
	}
}

// Data_reader monitors Redis pub-sub channels for profile-start events and data
// records. It packages each data record into a Point struct with the current
// profile number and writes it to a Go channel.
func data_reader(psc redis.PubSubConn, pnum0 int,
	evchan string, tags map[string]string) <-chan *client.Point {

	c := make(chan *client.Point, 1)

	go func() {
		defer close(c)
		var ev *dputil.Event

		tags["pnum"] = strconv.Itoa(pnum0)
		dr := dputil.DataRecord{}
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				if msg.Channel == evchan {
					ev = dputil.ParseEvent(string(msg.Data))
					if ev.Name == "profile:start" {
						tags["pnum"] = strconv.Itoa(int(ev.Attrs["pnum"].(int64)))
					}
				} else {
					err := json.Unmarshal(msg.Data, &dr)
					if err == nil {
						reformat_data(&dr)
						pt, err := client.NewPoint(dr.Source,
							tags,
							dr.Data.(map[string]interface{}),
							dr.T)
						if err == nil {
							c <- pt
						} else {
							log.Printf("Cannot create Point: %v", err)
						}
					} else {
						log.Printf("JSON decode error: %v", err)
					}
				}
			}
		}
	}()

	return c
}

var usage = `
Usage: dpdbstore [options] DBURL DBNAME

Read DP data from multiple Redis pub-sub channels and write the
data records to an InfluxDB database DBNAME at DBURL
`

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	user := flag.String("user", "", "Database user name")
	pword := flag.String("pword", "", "Database password")
	taglist := flag.String("tags", "", "Tags to add to every entry")
	pnum := flag.Int("pnum", 0, "Initial profile number")
	dchans := flag.String("dchans", "data.sci,data.eng",
		"Comma separated list of data pubsub channels")
	evchan := flag.String("evchan", "events.mmp", "Profile events pubsub channel")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	psconn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	cfg := client.HTTPConfig{
		Addr: args[0],
	}

	if *user != "" {
		cfg.Username = *user
		cfg.Password = *pword
	}

	cln, err := client.NewHTTPClient(cfg)
	if err != nil {
		log.Fatalf("Error creating InfluxDB Client: %v", err.Error())
	}
	defer cln.Close()

	tags := make(map[string]string)
	if *taglist != "" {
		for _, s := range strings.Split(*taglist, ",") {
			entry := strings.SplitN(s, "=", 2)
			if len(entry) == 2 {
				tags[entry[0]] = entry[1]
			}
		}
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			psc.Unsubscribe()
		}
	}()

	c := data_reader(psc, *pnum, *evchan, tags)
	for _, cname := range strings.Split(*dchans, ",") {
		psc.Subscribe(cname)
	}
	psc.Subscribe(*evchan)

	for pt := range c {
		bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
			Database:  args[1],
			Precision: "us",
		})
		bp.AddPoint(pt)
		err = cln.Write(bp)
		if err != nil {
			log.Println(err)
		}
	}
}
